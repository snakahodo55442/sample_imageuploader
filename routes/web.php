<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'UploadController@index');
Route::get('/home/{locale?}', 'UploadController@index');
Route::post('/upload/{locale?}', 'UploadController@upload');
Route::get('/calculate/{filename}/{locale?}', 'CalculateController@index');
Route::post('/execute/{locale?}', 'CalculateController@execute');
Route::post('/locale', 'LocaleController@index');
