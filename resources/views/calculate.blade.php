@extends('master')

@section('meta_specific')

<meta content="robots" name="noindex, nofollow" />

@stop

@section('content')

  <div class="container ens-contentbody">

    <div class="row ens-loading-box">

      <h3 class="ens-loading-title">@lang('labels.calculate.loading_title')</h3>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <img class="ens-loading" src="{!! URL::asset('img/assets/loading.gif') !!}" />
      </div>

    </div>

    <div class="row ens-result-box hidden">

      <h1 class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">@lang('labels.calculate.result_title')</h1>

      <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <img class="ens-result-img" src="{{ $uploaded_img }}" />
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3>Showing extracted text from the image</h3>
        <pre id="result-body"></pre>
      </div>

      <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12 ens-back-home">
        <a type="button" class="btn btn-primary ens-button-wide" href="@lang('labels.site.links.home')">@lang('labels.calculate.back_home')</a>
      </div>

    </div>

  </div>

  <div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">@lang('labels.calculate.error.title')</h4>
        </div>
        <div class="modal-body">

          <p class="">@lang('labels.calculate.error.body')</p>

        </div>
        <div class="modal-footer">
          <a type="button" class="btn btn-primary ens-button-wide" href="@lang('labels.site.links.home')">@lang('labels.calculate.error.button')</a>
        </div>
      </div>
    </div>
  </div>

  <input type="hidden" name="csrf-token" id="csrf-token" value="{{ csrf_token() }}">

@stop

@section('script')

<script src="{!! URL::asset('js/calculate.js') !!}"></script>

@stop
