@extends('master')

@section('meta_specific')

<meta content="robots" name="noindex, nofollow" />

@stop

@section('content')

<div class="container">

  <form id="upload-form" class="container-fluid row ens-contentbody" method="post" action="{{ $upload_location }}" enctype="multipart/form-data">

    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <label class="ens-center-text">@lang('labels.upload_form.sampledesc')</label>
      <img class="ens-uploadsample-img" src="{!! URL::asset('img/assets/upload_sample.jpg') !!}" />
    </div>

    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <label id="select-file-button" class="btn btn-default ens-select-button">
          @lang('labels.upload_form.file') <input id="select-file" type="file" class="ens-upload-input" name="target_file">
      </label>
      <p id="selected-msg" class="ens-center-text"></p>
    </div>
    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <input id="submit" type="submit" name="submit" class="btn btn-primary ens-upload-button" value="@lang('labels.upload_form.button')" disabled="disabled" >
    </div>
    <div class="form-group text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <a href="@lang('labels.site.links.othersite')">@lang('labels.nav.othersite')</a>
    </div>

  	{{ csrf_field() }}

  </form>

</div>

@stop

@section('script')

<script src="{!! URL::asset('js/upload.js') !!}"></script>

@stop
