<!DOCTYPE html>
<html>
  <head>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>@lang('labels.site.title')</title>
    <meta name="description" content="@lang('labels.site.description')">
    <meta name="author" content="@lang('labels.site.author')" />
    <meta name="keywords" content="@lang('labels.site.keywords')" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    @yield('meta_specific')

    <link href="{!! URL::asset('bootstrap/css/bootstrap.css') !!}" rel="stylesheet">
    <link href="{!! URL::asset('css/style.css') !!}" rel="stylesheet">

  </head>
  <body class="{{ $body_class }}">

    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed {{ $menu_visible }}" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="@lang('labels.site.links.home')">@lang('labels.title')</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right {{ $menu_visible }}">
            <li><a href="@lang('labels.site.links.home')">@lang('labels.nav.home')</a></li>
            <li><a href="@lang('labels.site.links.about')">@lang('labels.nav.about')</a></li>
            <li><a href="@lang('labels.site.links.author')">@lang('labels.nav.author')</a></li>
          </ul>
        </div>

      </div>

    </nav>

    @yield('content')

    <script src="{!! URL::asset('js/jquery-3.1.1.min.js') !!}"></script>
    <script src="{!! URL::asset('bootstrap/js/bootstrap.min.js') !!}"></script>

    @yield('script')

  </body>
</html>
