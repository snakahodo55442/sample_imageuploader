<?php

return [
	'title' => 'サンプル ライブ 火力計算',
	'nav' => [
		'home' => 'ホーム',
		'about' => '使い方',
		'author' => '製作者について',
		'back_home' => 'アップロード画面へ',
		'othersite' => 'English site'
	],
	'site' => [
		'title' => 'サンプル計算',
		'author' => '',
		'description' => '',
		'keywords' => '',
		'links' => [
			'home' => '/',
			'about' => '/howto/ja',
			'author' => '/author/ja',
			'othersite' => '/home/en'
		]
	],
	'upload_form' => [
		'sampledesc' => '画面をアップロードすると解析を開始します。',
		'file' => '画像を選択',
		'button' => 'アップロード',
		'button_uploading' => 'アップロードしています...',
		'invalid_filename' => '画像ではないファイルが選択されています。もう一度、画像を選択して下さい。'
	],
	'calculate' => [
		'loading_title' => '画像を解析しています',
		'result_title' => '解析結果',
		'error' => [
			'title' => 'エラー発生',
			'body' => '画像を解析出来ませんでした。もう一度時間をおいてアップロードするか、再度スクリーンショットを撮ってもう一度お試し下さい。',
			'button' => 'ホームに戻る'
		],
		'back_home' => 'ホームに戻って再アップロード'
	]
];
