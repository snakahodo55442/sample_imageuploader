<?php

return [
	'title' => 'Sample Live Calculator',
	'nav' => [
		'home' => 'Home',
		'about' => 'How it works',
		'author' => 'Author',
		'back_home' => 'Back to top',
		'othersite' => '日本語'
	],
	'site' => [
		'title' => 'sample',
		'author' => '',
		'description' => '',
		'keywords' => '',
		'links' => [
			'home' => '/home/en',
			'about' => '/howto/en',
			'author' => '/author/en',
			'othersite' => '/'
		]
	],
	'upload_form' => [
		'sampledesc' => 'Just upload any image and the app will detect text on it',
		'file' => 'Select image',
		'button' => 'Upload',
		'button_uploading' => 'Uploading...',
		'invalid_filename' => 'You must select an image'
	],
	'calculate' => [
		'loading_title' => 'Calculating...',
		'result_title' => 'Your Result',
		'error' => [
			'title' => 'Oops! Error occurred',
			'body' => 'An error occurred while analyzing your image. Try again after a few minutes, or retake a screenshot and upload it.',
			'button' => 'Back to home'
		],
		'back_home' => 'Upload another screenshot'
	],
];
