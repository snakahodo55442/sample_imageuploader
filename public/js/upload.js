$(function() {

	var locale_labels;

	var get_locale = function () {

		var result;

		var a = document.createElement('a');

		a.href = window.location.href;

		result = a.pathname.replace('/', '');

		return result;

	};

	var locale = get_locale;

	//get locale
	var locale_options = {
		type: 'post',
		url: '/locale',
		data: {lang: locale, parts: 'upload_form'},
		dataType: 'json',
		success: function(data) {

			locale_labels = data;

		}
	}

	$.ajax(locale_options);
	
	$('#select-file').on('change', function(e) {

		var filename = $(e.target).val().replace(/\\/g, '/').replace(/.*\//, '');

		if (filename.match(/.*(png|gif|jpeg|jpg)$/i)) {

			$('#selected-msg').removeClass('text-danger').text(filename);
			$('#submit').attr('disabled', false);

		} else {

			$('#selected-msg').addClass('text-danger').text(locale_labels.invalid_filename);
			$('#select-file').attr('enabled', true);
			$('#submit').attr('disabled', true);

		}

	});

	$('#upload-form').on('submit', function(e) {

		$('#submit').attr('disabled', true).val(locale_labels.button_uploading);

	});

});