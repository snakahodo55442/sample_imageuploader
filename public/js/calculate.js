$(function() {

	var locale_labels;
	var id;
	var type;

	var get_locale = function () {

		var result;

		var a = document.createElement('a');

		a.href = window.location.href;

		var match = a.pathname.match(/\/[a-z]+$/)

		if (!match) {

			result = 'ja';

		} else {

			result = match[0].replace('/', '');

		}

		return result;

	};

	var selected_locale = get_locale();

	var execute_location;

	if (get_locale() !== 'ja') {

		execute_location = '/execute/' + get_locale();

	} else {

		execute_location = '/execute/';

	}

	//get locale
	var locale_options = {
		type: 'post',
		url: '/locale',
		data: {lang: selected_locale, parts: 'calculate'},
		dataType: 'json',
		success: function(data) {

			locale_labels = data;

		}
	}

	$.ajax(locale_options);

	var parser = document.createElement('a');
	parser.href = location.href;

	var filename = parser.pathname.replace('/calculate/', '');
	filename = filename.replace(/\/[a-z]+$/, '');
	//filename = filename.replace('/', '');

	var calculate_options = {
		type: 'post',
		//url: '/execute',
		url: execute_location,
		headers: {
			'X-CSRF-TOKEN': $('#csrf-token').val()
		},
		data: {path: filename},
		dataType: 'json',
		success: function(data) {

			if (data.status === 'error') {

				$('#error-modal').modal('show');

			} else {

				$('#result-body').text(data);

				$('.ens-loading-box').addClass('hidden');
				$('.ens-result-box').removeClass('hidden');

			}

		},
		error: function() {

			$('#error-modal').modal('show');

		}
	};

	$.ajax(calculate_options);

});
