$(function() {

  var Carousel = $.fn.carousel;

	Carousel.TRANSITION_DURATION = 1000

	$.fn.carousel = function(options) {

		if (!options) {
			options = {};
		}

		if (!options.animations) {
			options.animations = [
				['left', 'left']
			]
		}

		Carousel.call(this, options);
	}

	$.fn.carousel.Constructor = Carousel.Constructor;

  var ext = {
  	slide: function (type, next) {
	    var $active   = this.$element.find('.item.active')
	    var $next     = next || this.getItemForDirection(type, $active)
	    var isCycling = this.interval
	    var direction = type == 'next' ? 'left' : 'right'
	    var that      = this

	    var inIdx = 0
	    var outIdx = 1
	    var animationIdx = Math.floor(Math.random() * this.options.animations.length)

	    var InAnimationType = [this.options.animations[animationIdx][inIdx], 'animated'].join(' ')
	    var OutAnimationType = [this.options.animations[animationIdx][outIdx], 'animated'].join(' ')

	    if ($next.hasClass('active')) return (this.sliding = false)

	    var relatedTarget = $next[0]
	    var slideEvent = $.Event('slide.bs.carousel', {
	      relatedTarget: relatedTarget,
	      direction: direction
	    })
	    this.$element.trigger(slideEvent)
	    if (slideEvent.isDefaultPrevented()) return

	    this.sliding = true

	    isCycling && this.pause()

	    if (this.$indicators.length) {
	      this.$indicators.find('.active').removeClass('active')
	      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
	      $nextIndicator && $nextIndicator.addClass('active')
	    }

	    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
	    if ($.support.transition && this.$element.hasClass('slide')) {
	    	//removes default position
	      $next.addClass(type)
	      	.css('left', 0)
	      	.css('-webkit-transition', 'none')
	      	.css('-o-transition', 'none')
	      	.css('transition', 'none')
	      $next[0].offsetWidth // force reflow
	      $active.addClass(OutAnimationType)
	      $next.addClass(InAnimationType)
	      $active
	        .one('bsTransitionEnd', function () {
	          $next.removeClass([type, InAnimationType].join(' ')).addClass('active')
	          $active.removeClass(['active', OutAnimationType].join(' '))
	          that.sliding = false
	          setTimeout(function () {
	            that.$element.trigger(slidEvent)
	          }, 0)
	        })
	        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
	    } else {
	      $active.removeClass('active')
	      $next.addClass('active')
	      this.sliding = false
	      this.$element.trigger(slidEvent)
	    }

	    isCycling && this.cycle()

	    return this
	  }
  }

  $.extend(true, $.fn.carousel.Constructor.prototype, ext);
	
});
