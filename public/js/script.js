$(function() {

  $('#maincarousel').carousel({
    animations: [
      /*
      */
      ['slideInUp', 'slideOutUp'],
      ['slideInDown', 'slideOutDown'],
      ['slideInLeft', 'slideOutRight'],
      ['slideInRight', 'slideOutLeft'],
      ['rotateIn', 'rotateOut'],
      ['flipInX', 'flipOutX'],
      ['fadeInLeft', 'fadeOutRight'],
      ['fadeInRight', 'fadeOutLeft'],
      ['fadeInUp', 'fadeOutUp'],
      ['fadeInDown', 'fadeOutDown'],
      //['lightSpeedIn', 'lightSpeedOut'],
      //['lightSpeedIn', 'flash']
    ]
  });

  $('#s-navitems').on('activate.bs.scrollspy', function(e) {

    var target = $($(e.target)['context'].innerHTML).attr('href');

    if (target == '#s-introduction') {

      $('#s-skillsets').find('.s-section-title').triggerAnimate();
      $('#s-skillsets').find('.s-section-description').triggerAnimate();

      $('#s-skillsets').find('.s-skill-title.s-php').triggerAnimate(200);
      $('#s-skillsets').find('.s-skill-title.s-javascript').triggerAnimate(300);
      $('#s-skillsets').find('.s-skill-title.s-html').triggerAnimate(400);
      $('#s-skillsets').find('.s-skill-title.s-tools').triggerAnimate(500);

      $('#s-skillsets').find('.s-skill-list.s-php').triggerAnimate(1000);
      $('#s-skillsets').find('.s-skill-list.s-javascript').triggerAnimate(1100);
      $('#s-skillsets').find('.s-skill-list.s-html').triggerAnimate(1200);
      $('#s-skillsets').find('.s-skill-list.s-tools').triggerAnimate(1300);

    } else if (target == '#s-skillsets') {

      $('#s-my-background').find('.s-section-title').triggerAnimate();
      $('#s-my-background').find('.s-section-description').triggerAnimate();

      $('#s-my-background').find('.s-mybackground-item.s-okinawa .s-mybackground-image').triggerAnimate(200);
      $('#s-my-background').find('.s-mybackground-item.s-flight .s-mybackground-image').triggerAnimate(300);
      $('#s-my-background').find('.s-mybackground-item.s-melbourne .s-mybackground-image').triggerAnimate(400);
      $('#s-my-background').find('.s-mybackground-item.s-sap .s-mybackground-image').triggerAnimate(500);

      $('#s-my-background').find('.s-mybackground-item.s-okinawa .s-mybackground-caption').triggerAnimate(1000);
      $('#s-my-background').find('.s-mybackground-item.s-flight .s-mybackground-caption').triggerAnimate(1100);
      $('#s-my-background').find('.s-mybackground-item.s-melbourne .s-mybackground-caption').triggerAnimate(1200);
      $('#s-my-background').find('.s-mybackground-item.s-sap .s-mybackground-caption').triggerAnimate(1300);

    } else if (target == '#s-my-background') {

      $('#s-footer').find('.s-section-title').triggerAnimate();
      $('#s-footer').find('.s-section-description').triggerAnimate();

      $('#s-footer').find('.s-footer-item.s-facebook').triggerAnimate(100);
      $('#s-footer').find('.s-footer-item.s-twitter').triggerAnimate(300);
      $('#s-footer').find('.s-footer-item.s-linkedin').triggerAnimate(500);

      $('#s-footer').find('.s-footer-item.s-phone').triggerAnimate(700);
      $('#s-footer').find('.s-footer-item.s-divider').triggerAnimate(900);
      $('#s-footer').find('.s-footer-item.s-email').triggerAnimate(1100);

    }

  });

  $('.s-social-icon').on('mouseover', function(e) {
    $(e.target).addClass('animated pulse');
  });

  $('.s-social-icon').on('mouseout', function(e) {
    $(e.target).removeClass('animated pulse');
  });

});
