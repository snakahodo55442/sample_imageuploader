<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnstatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      Schema::drop('enstats');
      Schema::drop('unit_skills');

      Schema::create('enstats', function(Blueprint $table) {
        
        $table->increments('id');
        $table->text('filepath');
        $table->tinyInteger('deck_type')->comment('1=da, 2=vo, 3=pf');
        $table->integer('data_main')->unsigned();
        $table->integer('data_sub1')->unsigned();
        $table->integer('data_sub2')->unsigned();
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
        $table->tinyInteger('status')->default(1);

      });

      Schema::create('unit_skills', function(Blueprint $table) {
        
        $table->increments('id');
        $table->integer('enstats_id')->unsigned();
        $table->tinyInteger('unit_type')->comment('1=main, 2=sub1, 3=sub2');
        $table->tinyInteger('data')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
      Schema::drop('enstats');
      Schema::drop('unit_skills');

    }
}
