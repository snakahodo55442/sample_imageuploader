<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Library\GoogleApis;
use Storage;
use App;
use Lang;
use Log;

class UploadController extends Controller
{

	public function index(Request $request, $locale = null) {

		$lang;
		$upload_location;
		$menu_visible = '';

		if ($locale) {

			App::setLocale($locale);
			$upload_location = '/upload/' . $locale;
			$menu_visible = 'hidden';

		} else {

			$upload_location = '/upload/';

		}

		$view_vars = [
			'upload_location' => $upload_location,
			'body_class' => 'ens-decorated',
			'menu_visible' => $menu_visible
		];

    return view('upload', $view_vars);

  }

  public function upload(Request $request, $locale = null) {

  	if ($request->target_file) {

			$redirect_lang;

			if ($locale) {

				App::setLocale($locale);
				$redirect_lang = '/' . $locale;

			} else {

				$redirect_lang = '/';

			}

		  $is_image = getimagesize($request->target_file);

		  if ($is_image) {

		  	$filename = $request->target_file->store('', 'enstar');

				$googleApis = new GoogleApis();

				$path = $googleApis->upload_to_bucket($filename);

				if ($filename) {

					return redirect('calculate/' . $filename . $redirect_lang);

				}

		  }

  	}

  }

}
