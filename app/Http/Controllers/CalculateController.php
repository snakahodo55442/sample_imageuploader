<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\GoogleApis;
use App\Library\Analyser;
use App\Enstat;
use App\UnitSkill;
use Storage;
use App;
use Log;

class CalculateController extends Controller
{

	private $_unittype_main = 1;
	private $_unittype_sub1 = 2;
	private $_unittype_sub2 = 3;

	public function index(Request $request, $img, $locale = null) {

		$menu_visible = '';

		if ($locale) {

			App::setLocale($locale);
			$menu_visible = 'hidden';

		}

		//$view_vars = ['uploaded_img' => env('APP_URL') . '/img/ensemble_stars/' . $img];
		$view_vars = [
			'uploaded_img' => url('/img/ensemble_stars') . '/' . $img,
			'body_class' => '',
			'menu_visible' => $menu_visible
		];

		return view('calculate', $view_vars);

	}

	public function calctest(Request $request, $img) {

		$image_exists = Storage::disk('enstar')->exists($img);

		$googleApis = new GoogleApis();

		$result = $googleApis->image_text_detect($img);

	}

	public function execute(Request $request, $locale = null) {

		if ($locale) {

			App::setLocale($locale);

		}

		$result = [];

		//$image_exists = getimagesize(public_path('img/ensemble_stars') . '/' . $request->path);
		//$image_exists = Storage::disk('enstar')->exists($request->path);

		if ($request->path) {

			$googleApis = new GoogleApis();

			$result = $googleApis->image_text_detect($request->path);

			if (!$result) {

				$result = ['status' => 'error'];

			} else {

				//Skip database table insert process
				/*
				$enstat = new DatabaseIfExists();
				$enstat->filepath = $request->path;

				$enstat->save();
				*/

    	}

		} else {

			$result = ['status' => 'error'];

		}

		return response()->json($result);

	}

	//expecting [unit_skills: [main: [data: 100, percent: 13], [sub1: [data: 100, percent: 13]]
	public function execute_unitskills(Request $request, $locale = null) {

		if ($locale) {

			App::setLocale($locale);

		}

		$result = [];

		if ($request->id && $request->unit_skills) {

			$enstat = new Enstat();
			$stat = $enstat->find($request->id);

			$unit_skill_params = [];

			if (isset($request->unit_skills['unit_main'])) {

				foreach ($request->unit_skills['unit_main'] as $skillset) {

					$percent = (int) $skillset['percent'];

					if ($percent > 0) {

						$unitskill = new UnitSkill();
						$unitskill->enstats_id = $request->id;
						$unitskill->unit_type = $this->_unittype_main;
						$unitskill->data = $skillset['percent'];
						$unitskill->save();

						$value_result = (int) ($skillset['percent'] * (float) ($stat->data_main / 100));

						$unit_skill_params['unit_main'][] = [
							'unitname' => $skillset['unitname'],
							'result' => $value_result,
							'percent' => $skillset['percent']
						];

					}

				}

			}

			if (isset($request->unit_skills['unit_sub1'])) {

				foreach ($request->unit_skills['unit_sub1'] as $skillset) {

					$percent = (int) $skillset['percent'];

					if ($percent > 0) {

						$unitskill = new UnitSkill();
						$unitskill->enstats_id = $request->id;
						$unitskill->unit_type = $this->_unittype_sub1;
						$unitskill->data = $skillset['percent'];
						$unitskill->save();

						$value_result = (int) ($skillset['percent'] * (float) ($stat->data_sub1 / 100));

						$unit_skill_params['unit_sub1'][] = [
							'unitname' => $skillset['unitname'],
							'result' => $value_result,
							'percent' => $skillset['percent']
						];

					}

				}

			}

			if (isset($request->unit_skills['unit_sub2'])) {

				foreach ($request->unit_skills['unit_sub2'] as $skillset) {

					$percent = (int) $skillset['percent'];

					if ($percent > 0) {

						$unitskill = new UnitSkill();
						$unitskill->enstats_id = $request->id;
						$unitskill->unit_type = $this->_unittype_sub2;
						$unitskill->data = $skillset['percent'];
						$unitskill->save();

						$value_result = (int) ($skillset['percent'] * (float) ($stat->data_sub2 / 100));

						$unit_skill_params['unit_sub2'][] = [
							'unitname' => $skillset['unitname'],
							'result' => $value_result,
							'percent' => $skillset['percent']
						];

					}

				}

			}

			$base_data = [
				'unit_type' => $stat->deck_type,
				'unit_main' => $stat->data_main,
				'unit_sub1' => $stat->data_sub1,
				'unit_sub2' => $stat->data_sub2,
			];

			$result = Analyser::format_final_magnitudes($base_data, $unit_skill_params);

		} else {

			$result = ['total' => 'could not calculate'];

		}

		return response()->json($result);

	}

}
