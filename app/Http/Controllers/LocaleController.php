<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class LocaleController extends Controller
{

  public function index(Request $request) {

  	if ($request->lang && $request->parts) {

  		App::setLocale($request->lang);

  		$labels = __('labels.' . $request->parts);

  		return response()->json($labels);

  	}

  }

}
