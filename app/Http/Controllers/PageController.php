<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{

  public function howto() {

		$view_vars = [
			'body_class' => '',
			'menu_visible' => ''
		];

    return view('about', $view_vars);

  }
  
  public function author() {

		$view_vars = [
			'body_class' => 'ens-decorated',
			'menu_visible' => ''
		];

    return view('author', $view_vars);

  }

}
