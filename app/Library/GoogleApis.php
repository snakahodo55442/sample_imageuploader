<?php
namespace App\Library {

	use Illuminate\Support\Facades\Cache;
	use Google\Cloud\ServiceBuilder;
	use Google\Cloud\Vision\VisionClient;
	use Google\Cloud\Storage\StorageClient;
	use Log;

	class GoogleApis {

		private $_projectId = 'whyskycola';
		private $_devKey = '';
		private $_bucketName = 'whiskycola';
		private $_bucketPath = 'gs://whiskycola/';
		private $_vision = null;
		private $_storage = null;

		public function __construct() {

			$gcloud = new ServiceBuilder([
				'keyFilePath' => base_path() . '/googlekey.json',
				'projectId' => $this->_projectId
			]);

			$this->_vision = $gcloud->vision();
			$this->_storage = $gcloud->storage();

		}

		public function image_text_detect($filename) {

			$image_url = $this->_bucketPath . $filename;
	    $this->_storage->registerStreamWrapper();

		  $image = $this->_vision->image(file_get_contents($image_url), ['TEXT_DETECTION']);
		  $result = $this->_vision->annotate($image);

		  $result_stats = [];

		  $vision_result = (array) $result->text();
			$description = [];

			foreach ($vision_result as $result_item) {

	  		array_push($description, $result_item->description());
				Log::debug($description);

		  }

		  return $description;

		}

		public function upload_to_bucket($filename) {

	    $bucket = $this->_storage->bucket($this->_bucketName);

		 	$file_path = public_path('img/ensemble_stars/') . $filename;

    	$bucket->upload(fopen($file_path, 'r'));

	    $this->_storage->registerStreamWrapper();

	    return $this->_bucketPath . $filename;

		}

	}

}
